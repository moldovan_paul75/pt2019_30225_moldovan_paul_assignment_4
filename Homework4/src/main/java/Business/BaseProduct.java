package Business;

public class BaseProduct extends MenuItem{
	
	private String name;
	private double price;
	
	public BaseProduct(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public double computePrice() {
		return this.price;
	}
	
	public String getName() {
		return this.name;
	}
	
	@Override
	public String toString() {
		return "\n" + this.name + ": "+ this.price+"\n";
	}

	public void add(MenuItem item) {
		//empty
	}

	public void remove(MenuItem item) {
		//empty	
	}

	public MenuItem getItem(int i) {
		return this;
	}
}
