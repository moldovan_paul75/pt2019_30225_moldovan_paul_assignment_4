package Business;

import java.text.DecimalFormat;
import java.util.*;

public class CompositeProduct extends MenuItem{
	
	private String name;
	private double price;
	private List<MenuItem> productList = new ArrayList<MenuItem>();
	
	public CompositeProduct(String name) {
		this.name = name;
		this.price = 0;
	}
	
	public void add(MenuItem item) {
		productList.add(item);
		computePrice();
	}

	public void remove(MenuItem item) {
		productList.remove(item);
		computePrice();
	}

	public double computePrice() {
		double p = 0;
		for(MenuItem i : productList)
			p+=i.computePrice();
		this.price = p;
		return this.price;
	}
	
	public MenuItem getItem(int i) {
		return productList.get(i);
	}

	public String getName() {
		return this.name;
	}
	
	@Override
	public String toString() {
		String print = "";
		
		print+="\n"+this.name+"\n\n";
		for(MenuItem i : productList)	print+=i;
		//print+="\nTotal price: "+ new DecimalFormat("#.##").format(this.price)+"\n--------------------------------"+"\n";
		print+="\n";
		return print;
	}


	
}
