package Business;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import Data.FileWriter;
import Data.RestaurantSerializator;
import Presentation.ChefGUI;

public class Restaurant extends Observable{
	
	Collection<MenuItem> menu = null;
	Map<Order, Collection<MenuItem>> orders = null;
	
	public Restaurant(){
		menu = new HashSet<MenuItem>();
		orders = new HashMap<Order, Collection<MenuItem>>();
		this.readMenu();
	}
	
	public void addOrder(Order o) {
		orders.put(o, o.getMenu());
        setChanged();
        notifyObservers(orders);
	}
	
	public void deleteOrder(Order o) {
		orders.remove(o);
        setChanged();
        notifyObservers(orders);
	}
	
	public void addItem(MenuItem i) {
		menu.add(i);
		this.writeMenu();
	}
	
	public void deleteItem(String name) {
		menu.remove(getItem(name));
		this.writeMenu();
	}
	
	public Collection<MenuItem> getMenu(){
		return this.menu;
	}
	
	public Map<Order, Collection<MenuItem>> getOrders(){
		return this.orders;
	}
	
	public MenuItem getItem(String name) {
		for(MenuItem i : menu) {
			if(i.getName().equals(name)) return i;
		}
		return null;
	}
	
	public void printMenu() {
		for(MenuItem i : menu) System.out.println(i);
	}
	
	public void writeMenu() {
		new FileWriter((HashSet<MenuItem>)menu);
	}
	
	public void readMenu() {
		menu = new RestaurantSerializator().getMenu();
	}
	
	public Order getOrder(int id) {
		for(Order o : orders.keySet()) {
			if(o.getOrderID() == id) return o;
		}
		return null;
	}
	
	public void printOrders() {
		for(Order o : orders.keySet()) {
			System.out.println(o + " " + orders.get(o) + "\nTotal: " + new DecimalFormat("#.##").format(o.getTotal()));
		}
	}

}
