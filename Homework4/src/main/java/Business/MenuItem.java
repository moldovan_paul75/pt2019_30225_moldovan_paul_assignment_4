package Business;

import java.io.Serializable;

public abstract class MenuItem implements Serializable{
	
	public abstract void add(MenuItem item);
	public abstract void remove(MenuItem item);
	
	public abstract double computePrice();
	public abstract String getName();
	
	public abstract MenuItem getItem(int i);
	public abstract String toString();
}
