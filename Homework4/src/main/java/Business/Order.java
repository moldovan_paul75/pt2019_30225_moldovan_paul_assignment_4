package Business;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class Order {
	
	private static int count = 1;
	
	public int OrderID;
	public Date date;
	public int tableID;	
	Collection<MenuItem> menu;
	
	
	public Order(int table) {
		OrderID = count;
		count++;
		date = new Date();
		tableID = table;
		menu = new HashSet<MenuItem>();
	}
	
	public int getTable() {
		return this.tableID;
	}
	
	public int getOrderID() {
		return this.OrderID;
	}
	
	public Date getDate() {
		return this.date;
	}
	
	public void addProduct(MenuItem i) {
		menu.add(i);
	}
	
	public void removeProduct(MenuItem i) {
		menu.remove(i);
	}
	
	public Collection<MenuItem> getMenu(){
		return this.menu;
	}
	
	public double getTotal() {
		double price = 0;
		for(MenuItem i : menu) {
			price += i.computePrice();
		}
		return price;
	} 
	
	@Override
	public String toString() {
		return "Order id: " + OrderID + ", table: " + tableID + ", date: " + date + "\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + OrderID;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((menu == null) ? 0 : menu.hashCode());
		result = prime * result + tableID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (OrderID != other.OrderID)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (menu == null) {
			if (other.menu != null)
				return false;
		} else if (!menu.equals(other.menu))
			return false;
		if (tableID != other.tableID)
			return false;
		return true;
	}
}
