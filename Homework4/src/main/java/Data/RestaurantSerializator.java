package Data;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;

import Business.MenuItem;

public class RestaurantSerializator {

	
	private FileInputStream fileIn = null;
	private ObjectInputStream in = null;
	private Collection<MenuItem> menu = null;
	
	public RestaurantSerializator() {
	      try {
	          FileInputStream fileIn = new FileInputStream("test.ser");
	          ObjectInputStream in = new ObjectInputStream(fileIn);
	          menu = (HashSet<MenuItem>) in.readObject();
	          in.close();
	          fileIn.close();
	       } catch (IOException i) {
	          i.printStackTrace();
	          return;
	       } catch (ClassNotFoundException c) {
	          c.printStackTrace();
	          return;
	       }
	}
	
	public Collection<MenuItem> getMenu(){
		return this.menu;
	}
}
