package Data;

import java.io.*;
import java.util.HashSet;

import Business.MenuItem;

public class FileWriter {
	
	private FileOutputStream fileOut = null;
	private ObjectOutputStream out = null;
	 
	public FileWriter(HashSet<MenuItem> menu) {
		  try {
		       FileOutputStream fileOut = new FileOutputStream("test.ser");
		       ObjectOutputStream out = new ObjectOutputStream(fileOut);
		       out.writeObject(menu);
		       out.close();
		       fileOut.close();
		    } catch (IOException i) {
		       i.printStackTrace();
		    }
	}
}
