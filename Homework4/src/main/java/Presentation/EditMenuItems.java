package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.Collection;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Business.CompositeProduct;
import Business.MenuItem;
import Business.Restaurant;

public class EditMenuItems extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
	private JTable table = null;
	private DefaultTableModel model = null;
	
	private JButton backBtn = null;
	private JButton saveBtn = null;
	private JButton deleteBtn = null;
	private JButton showMenuBtn = null;
	
	private Collection<MenuItem> menu = null;
	private String itemName = null;
	private double itemPrice = -1;
	private int row = -1;
	
	private static Restaurant res = null;
	
	public static void newEditItemsWindow(final Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					res = r;
					EditMenuItems frame = new EditMenuItems();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public EditMenuItems() {
		
		setBounds(100, 100, 405, 294);
    	setTitle("Products");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		table = new JTable();
		model = (DefaultTableModel) table.getModel();
		model.addColumn("Nume");
		model.addColumn("Pret");
		
		menu = res.getMenu();
		
		for(MenuItem i : menu) {
			model.addRow(new Object[] {i.getName(),  new DecimalFormat("#.##").format(i.computePrice())});
		}
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				getTableRow(e);
			}
		});
		
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				AdministratorGUI.newAdministratorWindow(res);
			}
		});
		
		showMenuBtn = new JButton("Show Menu");
		showMenuBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(itemName != null) {
					MenuItem i = res.getItem(itemName);
					if(i != null && i instanceof CompositeProduct) JOptionPane.showMessageDialog(null, i.toString());  
					else JOptionPane.showMessageDialog(null, "Not a composite product"); 
					//dispose();
					//CompositeMenuGUI.newCompositeItemsWindow();
				}
			}
		});
		
		saveBtn = new JButton("Save");
		saveBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				//todo
			}
		});
		
		
		deleteBtn = new JButton("Delete");
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(itemName != null) {
					new Restaurant().deleteItem(itemName);
					model.removeRow(row);
					JOptionPane.showMessageDialog(null, "Success"); 
				}
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		contentPane2.add(backBtn);
		contentPane2.add(saveBtn);
		contentPane2.add(deleteBtn);
		contentPane2.add(showMenuBtn);
		
		contentPane.add(scrollPane);
		contentPane.add(contentPane2);
		setContentPane(contentPane);
	}
	
	
	private void getTableRow(MouseEvent e) {
		row = table.getSelectedRow();
		itemName = table.getModel().getValueAt(row, 0).toString();
  		itemPrice = Double.parseDouble(table.getModel().getValueAt(row, 1).toString());
	}
}
