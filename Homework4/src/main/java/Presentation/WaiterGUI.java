package Presentation;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Business.Restaurant;

public class WaiterGUI extends JFrame{
	
	private JPanel contentPane = null;
	private JButton newOrderBtn = null;
	private JButton viewOrdersBtn = null;
	private JButton backBtn = null;
	
	static Restaurant res = null;
	
	public static void newWaiterWindow(final Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					res = r;
					WaiterGUI frame = new WaiterGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public WaiterGUI(){
		
		setBounds(100, 100, 405, 294);
    	setTitle("WaiterWindow");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(2, 3, 5, 5));
		
		JPanel contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(85, 5, 5, 5));
		contentPane2.setLayout(new BoxLayout(contentPane2, BoxLayout.PAGE_AXIS));
		
		newOrderBtn = new JButton("Create order");
		newOrderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				CreateOrderGUI.newCreateOrderWindow(res);
			}
		});
		contentPane.add(newOrderBtn);
		
		
		viewOrdersBtn = new JButton("Orders");
		viewOrdersBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				ShowOrdersGUI.newOrdersWindow(res);
			}
		});
		contentPane.add(viewOrdersBtn);
		
		
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow(res);
			}
		});
		contentPane2.add(backBtn);
		contentPane.add(contentPane2);
		
		setContentPane(contentPane);
		
	}
}
