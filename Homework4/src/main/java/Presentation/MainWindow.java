package Presentation;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Business.Restaurant;

public class MainWindow extends JFrame{
	
	private JPanel contentPane = null;
	private JButton administratorBtn = null;
	private JButton chefBtn = null;
	private JButton waiterBtn = null;
	
	private static Restaurant r = null;
	
	public static void newMainWindow(final Restaurant res) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					r = res;
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public MainWindow() {
		
		setBounds(100, 100, 405, 294);
    	setTitle("Main");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(2, 3, 5, 5));
		
		administratorBtn = new JButton("Administrator");
		administratorBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				AdministratorGUI.newAdministratorWindow(r);
			}
		});
		contentPane.add(administratorBtn);
		
		
		waiterBtn = new JButton("Waiter");
		waiterBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				WaiterGUI.newWaiterWindow(r);
			}
		});
		contentPane.add(waiterBtn);
		
		
		chefBtn = new JButton("Chef");
		chefBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				//dispose();
				ChefGUI.newChefWindow(r);
			}
		});
		contentPane.add(chefBtn);
		
		
		setContentPane(contentPane);
	}
}
