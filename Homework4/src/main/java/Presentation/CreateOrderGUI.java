package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.Collection;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Business.MenuItem;
import Business.Order;
import Business.Restaurant;

public class CreateOrderGUI extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	private JPanel contentPane3 = null;
	private JButton backBtn = null;
	private JButton addBtn = null;
	private JButton makeOrderBtn = null;
	private JButton createOrderBtn = null;
    private JTextField tableId = null;
	
	private JTable table = null;
	private DefaultTableModel model = null;
	
	private Collection<MenuItem> menu = null;
	private static Restaurant res = null;
	private Order order = null; 
	
	private String itemName = null;
	private double itemPrice = -1;
	private int row = -1;
	
	public static void newCreateOrderWindow(final Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					res = r;
					CreateOrderGUI frame = new CreateOrderGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public CreateOrderGUI() {
		
		setBounds(100, 100, 405, 294);
    	setTitle("New order");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		contentPane3 = new JPanel();
		contentPane3.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane3.setLayout(new FlowLayout());
		
		JTextField txt1 = new JTextField();
		txt1.setText("Table id:");
		txt1.setEditable(false);
		tableId = new JTextField(30);
		
		table = new JTable();
		model = (DefaultTableModel) table.getModel();
		model.addColumn("Nume");
		model.addColumn("Pret");
		
		
		menu = res.getMenu();
		
		for(MenuItem i : menu) {
			model.addRow(new Object[] {i.getName(),  new DecimalFormat("#.##").format(i.computePrice())});
		}
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				getTableRow(e);
			}
		});
		
		
		createOrderBtn = new JButton("Create order");
		createOrderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int id = -1;
				id = Integer.parseInt(tableId.getText());
				if(id != -1) {
					tableId.setText("");
					order = new Order(id);
					JOptionPane.showMessageDialog(null, "Success");
				}
			}
		});
		
		
		addBtn = new JButton("Add product to order");
		addBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(itemName != null && itemPrice != -1) {
					MenuItem i = null;
					i = res.getItem(itemName);
					if(i != null) {
						order.addProduct(i);
					}
				}
			}
		});
		
		makeOrderBtn = new JButton("Make order");
		makeOrderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(order != null) {
					res.addOrder(order);
					order = null;
					//res.printOrders();
					JOptionPane.showMessageDialog(null, "Success");
				}
			}
		});
		
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				WaiterGUI.newWaiterWindow(res);
			}
		});
		
		contentPane3.add(txt1); 
		contentPane3.add(tableId);
		
		
		JScrollPane scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane);
		
		contentPane2.add(backBtn);
		contentPane2.add(makeOrderBtn);
		contentPane2.add(createOrderBtn);
		contentPane2.add(addBtn);
		
		
		contentPane.add(contentPane3);
		contentPane.add(contentPane2);
		setContentPane(contentPane);	
	}
	
	private void getTableRow(MouseEvent e) {
		row = table.getSelectedRow();
		itemName = table.getModel().getValueAt(row, 0).toString();
  		itemPrice = Double.parseDouble(table.getModel().getValueAt(row, 1).toString());
	}
}