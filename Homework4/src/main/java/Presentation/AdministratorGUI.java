package Presentation;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Business.Restaurant;

public class AdministratorGUI extends JFrame{
	
	private JPanel contentPane = null;
	private JButton createBtn = null;
	private JButton menuBtn = null;
	private JButton backBtn = null;
	
	private static Restaurant res = null;
	
	public static void newAdministratorWindow(final Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					res = r;
					AdministratorGUI frame = new AdministratorGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public AdministratorGUI(){

		setBounds(100, 100, 405, 294);
    	setTitle("AdministratorWindow");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(2, 3, 5, 5));
		
		JPanel contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(85, 5, 5, 5)); 
		contentPane2.setLayout(new BoxLayout(contentPane2, BoxLayout.PAGE_AXIS));
		
		createBtn = new JButton("Create menu item");
		createBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				CreateMenuItemGUI.newMenuItemWindow(res);
			}
		});
		contentPane.add(createBtn);
		
		
		menuBtn = new JButton("Delete/edit menu items");
		menuBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				EditMenuItems.newEditItemsWindow(res);
			}
		});
		contentPane.add(menuBtn);
		
		
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow(res);
			}
		});
		contentPane2.add(backBtn);
		contentPane.add(contentPane2);
		
		setContentPane(contentPane);
	}
}
