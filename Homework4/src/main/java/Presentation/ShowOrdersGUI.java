package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Business.MenuItem;
import Business.Order;
import Business.Restaurant;

public class ShowOrdersGUI extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	private JButton backBtn = null;
	private JButton addBtn = null;
	private JButton showOrderBtn = null;
	private JButton billBtn = null;
	
	private JTable table = null;
	private DefaultTableModel model = null;
	
	private static Restaurant res = null;
	private Map<Order, Collection<MenuItem>> orders = null;
	
	private int row = -1;
	private int orderId = -1;
	
	public static void newOrdersWindow(final Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					res = r;
					ShowOrdersGUI frame = new ShowOrdersGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public ShowOrdersGUI() {
		
		setBounds(100, 100, 405, 294);
    	setTitle("Orders");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		
		table = new JTable();
		model = (DefaultTableModel) table.getModel();
		model.addColumn("Order id");
		model.addColumn("Table");
		model.addColumn("Date");
		model.addColumn("Products");
		model.addColumn("Price");
		
		orders = res.getOrders();
		for(Order o : orders.keySet()) {
			model.addRow(new Object[] {o.getOrderID(), 
								       o.getTable(), 
								       o.getDate(), 
								       orders.get(o), 
								       new DecimalFormat("#.##").format(o.getTotal())});
		}
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				getTableRow(e);
			}
		});
		
		
		showOrderBtn = new JButton("Show Order");
		showOrderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(orderId != -1) {
					Order o = res.getOrder(orderId);
					if(o != null) {
						JOptionPane.showMessageDialog(null, o.getMenu().toString()); 
						o = null;
					}
				}
			}
		});
		
		
		billBtn = new JButton("Bill");
		billBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				PrintWriter writer;
				try {
					if(orderId != -1) {
					     Order o = res.getOrder(orderId);
					     if(o != null) {
					     writer = new PrintWriter("bill.txt", "UTF-8");
					     writer.println(o.toString() + "\n" + o.getMenu().toString() + "\n\nTotal: " + o.getTotal());
					     orderId = -1;
					     JOptionPane.showMessageDialog(null, "Success"); 
					     res.deleteOrder(o);
					     model.removeRow(row);
					     o = null;
					     row = -1;
					     writer.close();
						}
					}
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				WaiterGUI.newWaiterWindow(res);
			}
		});
		contentPane2.add(backBtn);
		contentPane2.add(showOrderBtn);
		contentPane2.add(billBtn);
	
		
		JScrollPane scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane);
		
		contentPane.add(contentPane2);
		setContentPane(contentPane);
	}
	
	private void getTableRow(MouseEvent e) {
		row = table.getSelectedRow();
  		orderId = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
	}
	
}
