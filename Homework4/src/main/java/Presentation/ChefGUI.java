package Presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Business.MenuItem;
import Business.Order;
import Business.Restaurant;

public class ChefGUI extends JFrame implements Observer{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
	private JTable table = null;
	private DefaultTableModel model = null;
	
	private JButton showOrderBtn = null;
	
	private static Restaurant res = null;
	private Map<Order, Collection<MenuItem>> orders = null;
	
	private int row = -1; 
	private int orderId = -1;
	
	public static void newChefWindow(final Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					res = r;
					ChefGUI frame = new ChefGUI();
					res.addObserver(frame);
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public ChefGUI() {
		
		setBounds(100, 100, 405, 294);
    	setTitle("ChefWindow");
    	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(true);
    	setLocation(780, 220);
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		 
		table = new JTable();
		model = (DefaultTableModel) table.getModel();
		model.addColumn("Order id");
		model.addColumn("Table");
		model.addColumn("Date");
		model.addColumn("Products");
		model.addColumn("Price");	
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				getTableRow(e);
			}
		});
		
		
		showOrderBtn = new JButton("Show Order");
		showOrderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(orderId != -1) {
					Order o = res.getOrder(orderId);
					if(o != null) {
						JOptionPane.showMessageDialog(null, o.getMenu().toString()); 
						o = null;
					}
				}
			}
		});
		
		contentPane2.add(showOrderBtn);
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		contentPane.add(scrollPane);
		contentPane.add(contentPane2);
		
		setContentPane(contentPane);
	}

	private void getTableRow(MouseEvent e) {
		row = table.getSelectedRow();
  		orderId = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
	}
	
	public void update(Observable o, Object arg) {
		orders = (Map<Order, Collection<MenuItem>>) arg;
		model.setRowCount(0);
		for(Order order : orders.keySet()) {
		model.addRow(new Object[] {order.getOrderID(), 
								   order.getTable(), 
								   order.getDate(), 
							       orders.get(order), 
							       new DecimalFormat("#.##").format(order.getTotal())});
	}
		System.out.println("orders update");
	}
}
