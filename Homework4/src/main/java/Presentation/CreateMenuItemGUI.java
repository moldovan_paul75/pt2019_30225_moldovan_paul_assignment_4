package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Business.BaseProduct;
import Business.MenuItem;
import Business.Restaurant;

public class CreateMenuItemGUI extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	private JButton backBtn = null;
	private JButton addBtn = null;
	
    private JTextField itemName = null;
    private JTextField itemPrice = null;
	
    private String pName = null;
    private double price = -1;
    
    private static Restaurant res = null;
    
	public static void newMenuItemWindow(final Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					res = r;
					CreateMenuItemGUI frame = new CreateMenuItemGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	 
	public CreateMenuItemGUI() {
		
		setBounds(100, 100, 405, 294);
    	setTitle("New item");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout());
    	
    	contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new BoxLayout(contentPane2, BoxLayout.Y_AXIS));
		
		JPanel contentPane3 = new JPanel(new FlowLayout());
		JPanel contentPane4 = new JPanel(new FlowLayout());
		
		itemName = new JTextField(30);
		itemPrice = new JTextField(30);
		
		JTextField txt1 = new JTextField();
		txt1.setText("Product Name:");
		txt1.setEditable(false);
		
		JTextField txt2 = new JTextField();
		txt2.setText("Price:");
		txt2.setEditable(false);
		
		contentPane3.add(txt1);
		contentPane3.add(itemName);
		contentPane4.add(txt2);
		contentPane4.add(itemPrice);
		
		addBtn = new JButton("Add product");
		addBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				pName = itemName.getText();
				price = Double.parseDouble(itemPrice.getText());
				itemName.setText("");
				itemPrice.setText("");
				if(pName != null && price != -1) res.addItem(new BaseProduct(pName, price));
				JOptionPane.showMessageDialog(null, "Success");  	
			}
		});
		
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				AdministratorGUI.newAdministratorWindow(res);
			}
		});
		
		contentPane2.add(contentPane3);
		contentPane2.add(contentPane4);
		contentPane2.add(addBtn);
		contentPane2.add(backBtn);
		
		contentPane.add(contentPane2);
		setContentPane(contentPane);
	}
}
