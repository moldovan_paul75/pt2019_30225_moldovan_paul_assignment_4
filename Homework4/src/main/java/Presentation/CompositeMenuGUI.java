package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Business.MenuItem;

public class CompositeMenuGUI extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
	private JTable table = null;
	private DefaultTableModel model = null;
	
	private JButton backBtn = null;
	private JButton saveBtn = null;
	private JButton deleteBtn = null;
	
	//to do
	//edit composite item
	
	public static void newCompositeItemsWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompositeMenuGUI frame = new CompositeMenuGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public CompositeMenuGUI() {
		
		setBounds(100, 100, 405, 294);
    	setTitle("Composite Product");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		table = new JTable();
		model = (DefaultTableModel) table.getModel();
		model.addColumn("Nume");
		model.addColumn("Pret");
		

		
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				//EditMenuItems.newEditItemsWindow();
			}
		});
		
		saveBtn = new JButton("Save");
		saveBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				//todo
			}
		});
		
		
		deleteBtn = new JButton("Delete");
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//todo
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		contentPane2.add(backBtn);
		contentPane2.add(saveBtn);
		contentPane2.add(deleteBtn);
		
		contentPane.add(scrollPane);
		contentPane.add(contentPane2);
		setContentPane(contentPane);
	}
}
