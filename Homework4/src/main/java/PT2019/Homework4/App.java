package PT2019.Homework4;

import Business.Restaurant;
import Presentation.ChefGUI;
import Presentation.MainWindow;

public class App {
	
    public static void main( String[] args ){
		Restaurant r = new Restaurant();
		MainWindow.newMainWindow(r);
    }
}
