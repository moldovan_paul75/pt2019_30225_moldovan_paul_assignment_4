package PT2019.Homework4;

import Business.BaseProduct;
import Business.CompositeProduct;
import Business.MenuItem;
import Business.Order;
import Business.Restaurant;

public class OrderTest {
	
	public static void main(String[] args) {
		
		Restaurant r = new Restaurant();
		Order o = new Order(1);
		
		
//		MenuItem asdf = new BaseProduct("pizza casei", 14);
//		MenuItem asdf2 = new BaseProduct("cartofi prajiti", 3.99);
		MenuItem cola = new BaseProduct("coca-cola", 2.49);
//		
//		MenuItem asdf3 = new CompositeProduct("pizza menu");
//		asdf3.add(asdf);
//		asdf3.add(asdf2);
//		asdf3.add(cola);
//		
//		o.addProduct(asdf3);
		o.addProduct(cola);
		
		r.addOrder(o);
		r.printOrders();
	}
}
