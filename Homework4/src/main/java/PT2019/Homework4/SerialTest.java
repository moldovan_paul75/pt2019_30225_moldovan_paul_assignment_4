package PT2019.Homework4;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;

import Business.BaseProduct;
import Business.CompositeProduct;
import Business.MenuItem;
import Business.Restaurant;

public class SerialTest {
	   public static void main(String [] args) {
		   
		   
			MenuItem asdf = new BaseProduct("pizza casei", 14);
			MenuItem asdf2 = new BaseProduct("cartofi prajiti", 3.99);
			MenuItem cola = new BaseProduct("coca-cola", 2.49);
			
			MenuItem asdf3 = new CompositeProduct("pizza menu");
			asdf3.add(asdf);
			asdf3.add(asdf2);
			asdf3.add(cola);
			
			Restaurant r = new Restaurant();
			r.addItem(cola);
			r.addItem(asdf); 
			r.addItem(asdf2);
		   
		   MenuItem e = new BaseProduct("cola", 3.4);
		   
		      try {
		         FileOutputStream fileOut = new FileOutputStream("test.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(r.getMenu());
		         out.close();
		         fileOut.close();
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
		      
		      MenuItem e2 = null;
		      Collection<MenuItem> asd = null;
		      
		      try {
		          FileInputStream fileIn = new FileInputStream("test.ser");
		          ObjectInputStream in = new ObjectInputStream(fileIn);
		          asd = (HashSet<MenuItem>) in.readObject();
		          in.close();
		          fileIn.close();
		       } catch (IOException i) {
		          i.printStackTrace();
		          return;
		       } catch (ClassNotFoundException c) {
		          c.printStackTrace();
		          return;
		       }
		      System.out.println(asd);
		   }
}
